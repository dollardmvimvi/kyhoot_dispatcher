import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { apiServiceManager } from '../service/api-service-manager';
import { CookieService } from 'ngx-cookie-service';
import { stringify } from '@angular/compiler/src/util';
declare var jQuery: any;
declare var $: any;
declare var Fingerprint: any;
declare var moment :any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [apiServiceManager],
})


export class LoginComponent implements OnInit {
  userEmail: any;
  password: any;
  mailResponse_err: any;
  password_err: any;
  login_err: any;
  cookieValue = 'UNKNOWN';
  currentYear = moment().format('YYYY');
  constructor(private _apiServiceManager: apiServiceManager, private router: Router, private cookieService:CookieService) { }

  ngOnInit() {
    this.cookieValue = this.cookieService.get('user-token');
    //conole.log(this.cookieService.get('user-token'))
    // ////conole.log("the login credentials is :", this.cookieValue)
    if (this.cookieValue) {
      this.router.navigate(['dispatcher']);
    }
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: any) => {
        sessionStorage.setItem('latitude', position.coords.latitude);
        sessionStorage.setItem('longitude', position.coords.longitude);
        this.cookieService.set('latitude',position.coords.latitude);
        this.cookieService.set('longitude', position.coords.longitude);
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        ////conole.log("*********positioin***********", pos);
      });
    } else {
      sessionStorage.setItem('latitude', '12.9716');
      sessionStorage.setItem('longitude', '77.5946');
    }
    let ipAddr = this.cookieService.get('ip');
    if(!ipAddr){
      // $.get('https://ipapi.co/json').then((res)=>{
      //   this.cookieService.set('ip',res.ip);
      //   this.cookieService.set('countryCode', res.country);

      // }
      // ,error=>{
      //   $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
      //     this.cookieService.set('ip',data.ip);
      //   });
      // )
      $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
        //conole.log("login", data);
        sessionStorage.setItem('ip', data['ip'])
      });
      $.get('https://ipapi.co/json').then((res)=>{
        this.cookieService.set('ip',res.ip);
        this.cookieService.set('countryCode', res.country);

      })
      // //conole.log("login sessionStorage.getItem('ip')", sessionStorage.getItem('ip'));
      // sessionStorage.getItem('ip') ? this.cookieService.set('ip',sessionStorage.getItem('ip')) :'';
    }
  }
  ngOnDestroy() {
    $("app-login").hide();
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  checkEmail() {
    var mailResponse = this.validateEmail(this.userEmail);
    if (!mailResponse) {
      this.mailResponse_err = "Enter a valid Mail ID"
    };
  }

  clearAll() {
    this.mailResponse_err = '';
    this.password_err = '';
    this.login_err = '';
  }
  loginDisabled = false;
  loaderDisabled = false;
  dispatcherLogin() {
    this.loginDisabled = true;
    this.loaderDisabled = true;
    $('.form-control').addClass("loadinggif");
    var mailResponse = this.validateEmail(this.userEmail);
    if (!mailResponse) {
      $('.form-control').removeClass("loadinggif");
      this.mailResponse_err = "  Enter a valid Mail ID";
      this.loaderDisabled = false;
    } else if (!this.password) {
      $('.form-control').removeClass("loadinggif");
      this.password_err = " Password is Required";
      this.loaderDisabled = false;
    }

    if (this.password && mailResponse) {

      var data = {
        email: this.userEmail,
        password: this.password
      }

      this._apiServiceManager.login(data).subscribe(result => {
        ////conole.log("_apiServiceManager.login", result);
        this.loginDisabled = false;
        this.loaderDisabled = false;

        $('.form-control').removeClass("loadinggif");
        ////conole.log("log result", result);
        this.cookieService.set( 'user-token', result.data.token );
        this.cookieService.set( 'cityId', result.data.cityId );
        this.cookieService.set( 'loginUser', result.data.id );
        this.cookieService.set( 'CityName', result.data.cityName );
        this.router.navigate(['dispatcher']);
      }, error => {
        ////conole.log("err", error);
        this.loginDisabled = false;
        this.loaderDisabled = false;
        ////conole.log("error._body === String", typeof error['_body'] === "string")
        if(typeof error['_body'] === "string"){
          this.login_err = JSON.parse(error._body);
          //conole.log(JSON.parse(error._body))
        }
        else{
            ////conole.log("json err object",error.status);
            ////conole.log("login error", this.login_err);
            (error.status === 0)? this.login_err.message = "Please Check Interent Connection" : '';
        }
        $('.form-control').removeClass("loadinggif");
      }, () => {
        ////conole.log("completed");
      })
    }
  }


}