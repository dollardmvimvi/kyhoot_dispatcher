
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CountdownModule } from 'ngx-countdown';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { LoginComponent } from './login/login.component';
import { DispatcherComponent } from './dispatcher/dispatcher.component';
import { DispatcherHomeComponent } from './dispatcher/dispatcher-home/dispatcher-home.component';
import { TasksComponent } from './dispatcher/tasks/tasks.component';
import { AccountsComponent } from './dispatcher/accounts/accounts.component';
import { GodsviewComponent } from './dispatcher/godsview/godsview.component';
import { AppRoutingModule } from './app-routing.module';
import { mqttManager } from './service/mqtt-manager';
import { apiServiceManager } from './service/api-service-manager';
import { MatStepperModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { NgxScreenfullModule } from '@ngx-extensions/screenfull';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';


import {
  MatAutocompleteModule, MatButtonToggleModule, MatCardModule, MatChipsModule, MatDatepickerModule, MatDialogModule,
  MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
  MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule,
  MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule,
  MatToolbarModule, MatTooltipModule,MatFormFieldModule
} from '@angular/material';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DispatcherComponent,
    LoginComponent,
    DispatcherHomeComponent,
    GodsviewComponent,
    AccountsComponent,
    TasksComponent
  ],
  imports: [   
    BrowserModule,
    ToastrModule.forRoot(
      {
        timeOut: 1500,
        // positionClass: 'toast-bottom-right',
        preventDuplicates: true,
      }
    ),
    FlatpickrModule.forRoot(),
    NgxSmartModalModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CountdownModule,
    BrowserAnimationsModule,
    MatStepperModule,
    MatButtonModule, MatCheckboxModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    DragDropModule,
    NgxScreenfullModule,
    NgxMatIntlTelInputModule
    
  ],
  exports:[FormsModule,
  ReactiveFormsModule,NgxSmartModalModule],
  providers: [apiServiceManager, mqttManager,CookieService],
  bootstrap: [AppComponent],
 
})

export class AppModule { }   