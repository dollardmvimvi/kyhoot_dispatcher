import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatcherHomeComponent } from './dispatcher-home.component';

describe('DispatcherHomeComponent', () => {
  let component: DispatcherHomeComponent;
  let fixture: ComponentFixture<DispatcherHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
